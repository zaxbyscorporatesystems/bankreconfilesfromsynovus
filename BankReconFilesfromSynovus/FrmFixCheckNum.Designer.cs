﻿namespace BankReconFilesfromSynovus
{
    partial class FrmFixCheckNum
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFixCheckNum));
            this.label1 = new System.Windows.Forms.Label();
            this.cmdUpdate = new System.Windows.Forms.Button();
            this.dgrdErrors = new System.Windows.Forms.DataGridView();
            this.colRowId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCheck = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgrdErrors)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Firebrick;
            this.label1.Location = new System.Drawing.Point(32, 19);
            this.label1.MaximumSize = new System.Drawing.Size(400, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 40;
            this.label1.Text = "text";
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.Location = new System.Drawing.Point(177, 156);
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.Size = new System.Drawing.Size(189, 23);
            this.cmdUpdate.TabIndex = 39;
            this.cmdUpdate.Text = "Update Changed Check Number";
            this.cmdUpdate.UseVisualStyleBackColor = true;
            this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // dgrdErrors
            // 
            this.dgrdErrors.AllowUserToAddRows = false;
            this.dgrdErrors.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.dgrdErrors.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgrdErrors.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgrdErrors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrdErrors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colRowId,
            this.colCheck,
            this.colAmt});
            this.dgrdErrors.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgrdErrors.Location = new System.Drawing.Point(32, 68);
            this.dgrdErrors.MultiSelect = false;
            this.dgrdErrors.Name = "dgrdErrors";
            this.dgrdErrors.RowHeadersVisible = false;
            this.dgrdErrors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgrdErrors.Size = new System.Drawing.Size(334, 72);
            this.dgrdErrors.TabIndex = 38;
            // 
            // colRowId
            // 
            this.colRowId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.NullValue = null;
            this.colRowId.DefaultCellStyle = dataGridViewCellStyle2;
            this.colRowId.HeaderText = "Row";
            this.colRowId.Name = "colRowId";
            this.colRowId.ReadOnly = true;
            this.colRowId.Width = 54;
            // 
            // colCheck
            // 
            this.colCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Crimson;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.colCheck.DefaultCellStyle = dataGridViewCellStyle3;
            this.colCheck.HeaderText = "Check Number";
            this.colCheck.MaxInputLength = 10;
            this.colCheck.Name = "colCheck";
            // 
            // colAmt
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.colAmt.DefaultCellStyle = dataGridViewCellStyle4;
            this.colAmt.HeaderText = "Check Amount";
            this.colAmt.Name = "colAmt";
            this.colAmt.ReadOnly = true;
            // 
            // FrmFixCheckNum
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(399, 198);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmdUpdate);
            this.Controls.Add(this.dgrdErrors);
            this.ForeColor = System.Drawing.Color.Navy;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmFixCheckNum";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Correct Check Number";
            ((System.ComponentModel.ISupportInitialize)(this.dgrdErrors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdUpdate;
        private System.Windows.Forms.DataGridView dgrdErrors;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRowId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmt;
    }
}