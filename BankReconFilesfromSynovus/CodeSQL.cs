﻿using System;
using System.Data.SqlClient;
using System.Data;

namespace BankReconFilesfromSynovus
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    class CodeSQL : IDisposable
    {
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Commands the scalar.
        /// </summary>
        /// <param name="con">The SQL connection.</param>
        /// <param name="sqlCmd">The SQL to execute.</param>
        /// <returns>The first column of the first datarow.</returns>
        public object CmdScalar(string con, string sqlCmd)
        {
            object rslt = 0;
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlQ = new SqlCommand())
                    {
                        sqlQ.Connection = sqlCon;
                        sqlQ.CommandText = sqlCmd;
                        rslt = sqlQ.ExecuteScalar();
                    }
                    sqlCon.Close();
                    sqlCon.Dispose();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }

        /// <summary>
        /// Commands the dataset.
        /// </summary>
        /// <param name="con">The SQL connection.</param>
        /// <param name="sqlCmd">The SQL command that defines the dataset to extract.</param>
        /// <returns>The dataset base upon the input command property.</returns>
        public DataSet CmdDataset(string con, string sqlCmd)
        {
            DataSet dsRslt = new DataSet();
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlDataAdapter sqlDap = new SqlDataAdapter())
                    {
                        sqlDap.SelectCommand = new SqlCommand(sqlCmd, sqlCon);
                        sqlDap.Fill(dsRslt);
                    }
                    sqlCon.Close();
                    sqlCon.Dispose();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return dsRslt;
        }
        /// <summary>
        /// Commands the non query.
        /// </summary>
        /// <param name="con">The SQL connection.</param>
        /// <param name="sqlCmd">The SQL command.</param>
        /// <returns>The number of lines affected by the command.</returns>
        public int CmdNonQuery(string con, string sqlCmd)
        {
            int rslt = 0;
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlQ = new SqlCommand())
                    {
                        sqlQ.Connection = sqlCon;
                        sqlQ.CommandText = sqlCmd;
                        rslt = sqlQ.ExecuteNonQuery();
                    }
                    sqlCon.Close();
                    sqlCon.Dispose();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }
    }
}
