﻿using System;

namespace BankReconFilesfromSynovus
{
    class CodeGetTransactionDetails : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        //properties
        public string Rec88 { set; get; }
        public string AcctUnit { set; get; }
        public string DistAcct { set; get; }
        public string SubAcct { set; get; }
        public string BaiCode { set; get; }

        //method
        public void UnScramble88()
        {
            //int strtOfLast = 0;
            string tmp = string.Empty;
            DistAcct = "13002";     // changed 7/26/2016 per marc "10000";     // "13002";
            SubAcct = string.Empty;
            switch (BaiCode)
            {
                case "173":
                    DistAcct = "13002";
                    SubAcct = "7374";
                    break;
                case "206":
                    SubAcct = "7374";
                    if (Rec88.ToUpper().Contains("TRANSFER FROM DEPOSIT SYSTEM ACCOUNT"))
                    {
                        Rec88 = Rec88.Replace("TRANSFER FROM DEPOSIT SYSTEM ACCOUNT", "Tran from Dep Acct");
                    }
                    break;
                case "301":
                    Rec88 = Rec88.Replace("88,", "");
                    DistAcct = "13002";
                    SubAcct = "7374";
                    break;
                case "455":
                    if (Rec88.ToUpper().Contains("ZAXBYS INC ZAX SWEEPS"))
                    {
                        Rec88 = Rec88.Replace("ZAXBYS INC ZAX SWEEPS", "Zax Sweeps");
                        DistAcct = "10099";
                        AcctUnit = "900000";
                        // per Donna 8/5/2016       SubAcct = "4374";
                    }
                    break;
                //case "506":
                //    if (Rec88.ToUpper().Contains("TRANSFER TO DEPOSIT SYSTEM ACCOUNT"))
                //    {
                //        Rec88 = Rec88.Replace("TRANSFER TO DEPOSIT SYSTEM ACCOUNT", "Tran to Dep Acct");
                //    }
                //    break;
                case "698":
                    if (Rec88.ToUpper().Contains("ANALYSIS ACTIVITY"))
                    {
                        DistAcct = "51700";
                        AcctUnit = "900000";
                        SubAcct = string.Empty;
                    }
                    break;
                case "699":
                    break;
                default:
                    break;
            }
            Rec88 = Rec88.Replace("88,", "");
            Rec88 = Rec88.Trim().Length > 30 ? Rec88.Substring(0, 30).Trim() : Rec88.Trim();
            tmp = AcctUnit.Trim();
            if (tmp.Length > 0)
            {
                //using (CodeSqlCe clsSqlCe = new CodeSqlCe())
                using (CodeSQL clsSql = new CodeSQL())
                {
                    switch (tmp.Length)
                    {
                        case 1:
                            tmp = "0010" + tmp;
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                            break;
                        case 2:
                            tmp = "001" + tmp;
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                            break;
                        case 3:
                            tmp = "00" + tmp;
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                            break;
                        case 4:
                            tmp = $"{tmp}00";
                            break;
                        case 5:
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            break;
                        default:
                            break;
                    }
                }
            }
            AcctUnit = tmp;
        }
    }
}
